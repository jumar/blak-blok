<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Data Project</title>
</head>
<body>
    <h2>Edit Pegawai</h2>
    <a href="/KendaliProject/tabelProject">Kembali</a>

    <br>
    <br>
    @foreach ($projectList as $p)
    <form action="Update" method="post">
        {{ csrf_field() }}
        <table border="0">
            <tr>
            <td><input type="hidden" name="id" value="{{$p->Id_Kegiatan}}"></td>
            </tr>
            <tr>
                <td> Nama Project</td>
            <td><input type="text" name="namaKegiatan" required="required" value="{{$p->Nama_Kegiatan}}"></td>
            </tr>

            <tr>
                <td>Tempat Kegiatan</td> 
            <td><textarea name="tempatKegiatan" id="" cols="30" rows="10" required="required" value="{{$p->Tempat_Kegiatan}}" ></textarea>
            </tr>
            <tr>
                <td>Nilai Kegiatan</td> 
                <td><input type="text" name="nilaiKegiatan" required="required" value="{{$p->Nilai_Kegiatan}}"></td>
            </tr>
            <tr>
                <td>Tanggal Mulai</td> 
            <td><input type="text" name="tanggalMulai" required="required" value="{{$p->Tanggal_Mulai}}"></td>
            </tr>
            <tr> 
                <td> Tanggal Berakhir</td>
                <td><input type="text" name="tanggalAkhir" required="required" value="{{$p->Tanggal_Akhir}}"></td>
            </tr>
            <tr>
                <td>Status</td> 
                <td><input type="text" name="Status" required="required" value="{{$p->Status}}" ></td>
            </tr>
            <tr> 
                <td>keterangan</td> 
                <td><input type="text" name="Keterangan" required="required" value="{{$p->Keterangan}}" ></td>
            </tr>
            <tr>
                <td><input type="submit" value="Update Data"></td>
            </tr>
        </table>   
        </form> 
    @endforeach
    
</body>
</html>