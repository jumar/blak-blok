<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>List Project</title>
</head>
<a href="TambahPage" style="Underline"> Tambah Project Baru</a>
<body>
    <h3>Data Project</h3>
    <table border="1">
        
    <tr>
        <th>No</th>
        <th>Nama Kegiatan</th>
        <th>Tempat Kegiatan</th>
        <th>Nilai Kegiatan</th>
        <th>Tanggal Mulai</th>
        <th>Tanggal Akhir</th>
        <th>Status</th>
        <th>Keterangan</th>
    </tr> 
    @foreach($project as $p)
    <tr>
         
        <td>{{ $p ->Id_Kegiatan }}</td>
        <td>{{ $p ->Nama_Kegiatan }}</td>
        <td>{{ $p ->Nilai_Kegiatan }}</td>
        <td>{{ $p ->Tanggal_Mulai }}</td>
        <td>{{ $p ->Tanggal_Akhir }}</td>
        <td>{{ $p ->Status }}</td>
        <td>{{ $p ->Keterangan }}</td>
        <td><a href="editPage/{{$p->Id_Kegiatan}}">Edit</a> |
            <a href="HapusPage/{{$p->Id_Kegiatan}}">Hapus</a></td>
    </tr>
    @endforeach
    </table>
   
    <tr>
    </tr>
</body>
</html>