<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectListController extends Controller
{
    public function index(){
        return "halo";
    }

    public function LoadDB(){
    // menggambil Data dari dari tabel List Project
    $project = DB::table('ProjectList')->get();

    //mengirim data pegawai ke view index
    return view ('index',['project' => $project] );

    }

    public function tambahPage(){
        return view('Tambah');
    }

    public function addData(Request  $request){
        DB::table('ProjectList')->insert([
            'Nama_Kegiatan' => $request->namaKegiatan,
            'Tempat_Kegiatan' => $request->tempatKegiatan,
            'Nilai_Kegiatan' => $request->nilaiKegiatan,
            'Tanggal_Mulai' => $request->tanggalMulai,
            'Tanggal_Akhir' => $request->tanggalAkhir,
            'Status' => $request->Status,
            'Keterangan' => $request->Keterangan
        ]);
        return redirect('KendaliProject/tabelProject');
    }

    public function EditPage($id){

        //mengambil data pegawai berdasarkan id yang dipilih
        $projectList = DB::table('ProjectList')->where('Id_Kegiatan',$id)->get();

        //passing data pegawai yang di dapat di view blade
        return view('Edit',['projectList' => $projectList]);
    }

    public function UpdateData(Request $request){

        DB::table('ProjectList')->where('Id_Kegiatan', $request->Id_Kegiatan)->update([
            'Nama_Kegiatan' => $request->namaKegiatan,
            'Tempat_Kegiatan' => $request->tempatKegiatan,
            'Nilai_Kegiatan' => $request->nilaiKegiatan,
            'Tanggal_Mulai' => $request->tanggalMulai,
            'Tanggal_Akhir' => $request->tanggalAkhir,
            'Status' => $request->Status,
            'Keterangan' => $request->Keterangan
        ]);

        return redirect ('KendaliProject/tabelProject');
    }
}
