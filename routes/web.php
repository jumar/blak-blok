<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('halo', 'ProjectListController@index');

//route ke list daftar data
Route::get('KendaliProject/tabelProject','ProjectListController@LoadDB');
//ke halaman tambah
Route::get('KendaliProject/TambahPage','ProjectListController@tambahPage');
//eksekusi data edit
Route::get('KendaliProject/editPage/{Id_Kegiatan}','ProjectListController@EditPage');
//eksekusi tambah data
Route::post('KendaliProject/Tambahproject','ProjectListController@addData');
//eksekusi data update
Route::post('KendaliProject/editPage/Update','ProjectListController@UpdateData');

//Route::resourses('kendali_project', 'ProjectListController');